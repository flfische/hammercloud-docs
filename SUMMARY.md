# Summary
* [Introduction](README.md)
* [CHEP 2018](./conferences/CHEP2018/README.md)
   * [Improving ATLAS computing resource utilization with HammerCloud](./conferences/CHEP2018/CHEP2018-ATLAS-HC.md)
* [HC CCC testing](./ccc-testing-howto/README.md)
* [ATLAS ops](./atlas-ops/README.md)
  * [Massive blacklisting](./atlas-ops/massive_blacklisting.md)
  * [ALRBdevel tests](./atlas-ops/ALRBdevel.md)
  * [Check dataset cache](./atlas-ops/check_ds_cache.md)
  * [Self-approve a test](./atlas-ops/self_approve_test.md)
  * [Add/remove site to/from template](./atlas-ops/add_site_to_template.md)  
  * [Blacklisting study data](./atlas-ops/blacklisting_study_data.md)  
  * [HammerCloud extension](./atlas-ops/celery_prod/README.md)      
    * [Normal prio jobs](./atlas-ops/celery_prod/celery_prod.md)
    * [Low prio jobs](./atlas-ops/celery_prod/celery_prod_lowpriority.md)
    * [Template file configuration](./atlas-ops/celery_prod/celery_prod.md#template-file-configuration)
    * [Template migration](./atlas-ops/celery_prod/celery_prod.md#template-migration)
    * [Job priority](./atlas-ops/celery_prod/celery_prod.md#job-priority)
    * [Extraargs: override template configuration](./atlas-ops/celery_prod/celery_prod.md#override-template-configuration-with-extraargs)
    * [Input file randomization](./atlas-ops/celery_prod/celery_prod.md#input-file-randomization)
    * [Debug job configuration outside HC](./atlas-ops/celery_prod/celery_prod.md#debug-job-configuration-outside-hc)
    * [Debug template](./atlas-ops/debug_template_commissioning_test_submission.md)
  * [Commissioning/Decommissioning of AFTs and PFTs](./atlas-ops/aft_pft_commissioning_decommissioning.md)
* [ATLAS dev](./atlas-dev/README.md)
  * [Test lifetime](./atlas-dev/test_lifetime.md)
    * [Test generation](./atlas-dev/test_generation.md)
    * [Test submission](./atlas-dev/test_submission.md)
    * [Test monitoring loop](./atlas-dev/test_monitoring_loop.md)
  * [Miscellaneous](./atlas-dev/misc.md)
  * [ATLAS HC newcomer](./atlas-ops/newcomer.md)
* [CMS ops](./cms-ops/README.md)
  * [CRIC collector](./cms-ops/cric_collector.md)

* Obsolete
  * [Deploy Ganga patch](./atlas-ops/deploy_ganga_patch.md)
