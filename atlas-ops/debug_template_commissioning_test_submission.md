# Debug template commissioning / test submission

### Code repository
* Development is done in feature branch [see here](http://hcdocs.web.cern.ch/hcdocs/atlas-dev/misc.html#git-branches).
* Code is merged from feature branch into ``master`` branch
* RPM is built from ``master`` branch.
* hammercloud-atlas, master branch commits [https://gitlab.cern.ch/hammercloud/hammercloud-atlas/commits/master](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/commits/master)
   * rpm ``hammercloud-atlas`` 
* hammercloud-atlas-inputfiles, master branch commits [https://gitlab.cern.ch/hammercloud/hammercloud-atlas-inputfiles/commits/master](https://gitlab.cern.ch/hammercloud/hammercloud-atlas-inputfiles/commits/master)
   * rpm ``hammercloud-atlas-inputfiles`` 
* hammercloud-core, master branch commits [https://gitlab.cern.ch/hammercloud/hammercloud-core/commits/master](https://gitlab.cern.ch/hammercloud/hammercloud-core/commits/master)
   * rpms ``hammercloud-core`` and ``hammercloud-web`` 

### RPM repository
* [http://linuxsoft.cern.ch/internal/repos/ai6-testing/x86_64/os/Packages/](http://linuxsoft.cern.ch/internal/repos/ai6-testing/x86_64/os/Packages/)
   * RPM repository is available inside CERN perimeter network. Use proxy to access it: [https://twiki.cern.ch/twiki/bin/viewauth/IT/HammerCloudAgile#Accessing_local_webserver_from_o](https://twiki.cern.ch/twiki/bin/viewauth/IT/HammerCloudAgile#Accessing_local_webserver_from_o).

### Deployment
* Deployment is done via RPM. If you observe sudden issues with your new template file, make sure it is present and its content is the inteded one. 
* Commit & push & request merge to ``master`` to propagate your new template code to RPM.
* Templates on the submission/worker hosts are located in subdirectories of ``/data/hc/apps/atlas/inputfiles/templates/``.

### Test directory
* On the submission host (the one listed on the test page, e.g. ``hammercloud-ai-20`` for test [20102710](http://hammercloud.cern.ch/hc/app/atlas/test/20102710/)), the test directory is ``/data/hc/apps/atlas/testdirs/test_20102710/``.
* There is a ``stdouterr.txt`` file in the test directory. It contains ``info``+``error`` log messages for the test generation, submission, and monitoring loop.
   * Read here more about test lifetime: [http://hcdocs.web.cern.ch/hcdocs/atlas-dev/test_lifetime.html](http://hcdocs.web.cern.ch/hcdocs/atlas-dev/test_lifetime.html)

### Logs
* Variety of logs is available in several locations:
   * test directory, e.g. ``/data/hc/apps/atlas/testdirs/test_20102710/``
   * script logs, executed by local user ``hcuser``: ``/data/hc/apps/atlas/logs/``
   * script logs, executed by Celery worker: ``/data/hc/apps/atlas/logs.celery``
      * these logs are avaiable on  the VM where the Celery task was executed. 
         * For production machines this may be anywhere on the production cluster.
         * For development machines the Celery task is usually executed on the submission host. e.g. ``hammercloud-ai-05``, ``hammercloud-ai-05`` (former SLC6 dev nodes ``hammercloud-ai-20``, ``hammercloud-ai-07``, ``hammercloud-ai-08``).
   * Celery daemon worker logs, ``/var/log/celery/daemon.worker.celery.daemon.worker.celery.log``
* Script logs are flume-tailed to Timber. Search patterns in 
   ```
https://monit.cern.ch/app/kibana#/discover?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-15m,mode:quick,to:now))&_a=(columns:!(_source),index:'monit_prod_hammercloud_logs_*',interval:auto,query:(query_string:(analyze_wildcard:!t,query:'*')),sort:!(_score,desc))   
   ``` 

### Celery worker
* If Celery daemon worker logs is not incrementing, you may need to restart the celeryd service: 

   ```
# ssh your_user@hammercloud-ai-05
# sudo su
# service celeryd restart
   ```
* If there are recent ``raise get_redis_ConnectionError()`` errors after ``celeryd`` service restart in the Celery daemon worker log, please notify ``hammercloud-tickets@cern.ch``.
   * Best approach is to alert about the ongoing Redis connection issues, there is not much that can be done about connection issues a few hours or days ago. 


### Test termination
If you are debugging a template, or developing a new template, it is better to set the template to ``stress`` instead of functional, and switch back to functional when you are happy about the template. 

* In this way you will be able to force the test start and end datetimes, and it will allow you to run several tests of this template at the same time, you will not have to wait for the previous test to finish. 


When you are debugging a template, you should debug it on one of the dev submit nodes (e.g. ``hammercloud-ai-05`` or ``hammercloud-ai-06``), because you have sudo privileges there and can kill the processes of the running test. Dev submit nodes also have a special distinguished Celery queue, so every Celery task is executed on this particular machine, which is also a primary submit node for your test-in-development. 


If any of the test process killing does not help, you can always delete the particular faulty test from HC admin for good. 
