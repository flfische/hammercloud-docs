# CMS CRIC collector

## CRIC source
CMS HammerCloud collects topology information from CRIC: http://cms-cric.cern.ch/api/cms/site/query/?json

The fields we are interested in are 

* CMS Site name, 
* Tier level, 
* Facility (or Parent site or Region).

## CMS HC CRIC collector
Followed in [https://its.cern.ch/jira/browse/HC-979](https://its.cern.ch/jira/browse/HC-979).

### Field mapping
Mapping for the HC ``Site`` table: 

* ``CMS Site name`` to ``Site.name``,
* ``Tier level`` to ``Site.ddm``,
* ``Facility`` to ``Site.queue``.

### Cronjob
CMS HC CRIC collector runs on ``hammercloud-ai-80`` and runs once per day: 

```
  if ($::cron_cms_cric_collector) {
    if ($::cron_cms_cric_collector == 'true' ) {
      file { '/data/hc/apps/cms/logs/criclog':
        ensure=>directory,
      }

      ### run CMS CRIC collector once per day. HC-979
      cron{'CORE CMS CRIC collector':
        ensure  => 'present',
        user    => 'root',
        minute  => '0',
        hour    => '7',
        command => '/data/hc/apps/cms/scripts/server/cric_collector.sh &>/data/hc/apps/cms/logs/criclog/cric.log`date --iso-8601=seconds` ',
        #require => Package['hammercloud-cmscrab3']
      }

    }
  }

```
### Notifications
Failure notification is sent to ``hammercloud-notifications-cms@cern.ch``

Success notification (if a new site is added to HC DB Site table, or there is a difference between HC DB Site table and CRIC site json) is sent to ``hammercloud-notifications-cms@cern.ch`` and to ``cric-devs@cern.ch``.

### Logging
The CRIC collector script is logging to Timber. 

example URL:
```
https://monit-timber-hammercloud.cern.ch/kibana/app/kibana#/discover?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-24h,mode:quick,to:now))&_a=(columns:!(data.line,data.log_level,data.log_message),filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,index:AWHX5eBYEqCH7pJgsL4x,key:metadata.host,negate:!f,type:phrase,value:hammercloud-ai-80.cern.ch),query:(match:(metadata.host:(query:hammercloud-ai-80.cern.ch,type:phrase)))),('$state':(store:appState),meta:(alias:!n,disabled:!f,index:AWHX5eBYEqCH7pJgsL4x,key:data.filename,negate:!f,type:phrase,value:%2Fdata%2Fhc%2Fapps%2Fcms%2Flogs%2Fcric_collector.log),query:(match:(data.filename:(query:%2Fdata%2Fhc%2Fapps%2Fcms%2Flogs%2Fcric_collector.log,type:phrase))))),index:AWHX5eBYEqCH7pJgsL4x,interval:auto,query:(match_all:()),sort:!(metadata.timestamp,desc))
```

or

https://monit-timber-hammercloud.cern.ch/kibana/goto/71de62b97415826a87cf22e2ca48ee90

(with the short URL it is possible that you will have to change the time window selection)

Filters:

* ``metadata.host is hammercloud-ai-80.cern.ch``
* ``data.filename is /data/hc/apps/cms/logs/cric_collector.log``

